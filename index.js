import Game from '/src/game';

let canvas = document.getElementById("gamescreen");
let ctx = canvas.getContext("2d");

const GAME_WIDTH = 800;
const GAME_HEIGHT = 600;

ctx.clearRect(0, 0, GAME_WIDTH, GAME_HEIGHT);

let game = new Game(GAME_WIDTH, GAME_HEIGHT);

let last_time = 0;
function game_loop(time_stamp) {
  let delta_time = time_stamp - last_time;
  last_time = time_stamp;
  ctx.clearRect(0, 0, GAME_WIDTH, GAME_HEIGHT);

  game.update(delta_time);
  game.draw(ctx);

  requestAnimationFrame(game_loop);
}

requestAnimationFrame(game_loop);
