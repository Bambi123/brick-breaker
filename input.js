import Game from "./game";

export default class InputHandler {
  constructor(paddle, game) {
    document.addEventListener("keydown", (event) => {
      
      switch (event.keyCode) {
        case 37:
          paddle.move_left();
          break;
        case 39:
          paddle.move_right();
          break;
        case 65:
          paddle.move_left();
          break;
        case 68:
          paddle.move_right();
          break;
        case 27:
          game.toggle_pause();
          break;
        case 32:
            game.start();
            break;
        default:
          break;
      }
    });

    document.addEventListener("keyup", (event) => {
      
      switch (event.keyCode) {
        case 37:
          paddle.stop();
          break;
        case 39:
          paddle.stop();
          break;
        case 65:
          paddle.stop();
          break;
        case 68:
          paddle.stop();
          break;
        default:
          console.log(event.keyCode);
          break;
      }

    });
  }
}
