import {detect_collision} from "/src/collision_detection";

export default class Ball {

  constructor (game) {
    this.image = document.getElementById("img_ball");
    this.game_width = game.game_width;
    this.game_height = game.game_height;

    this.game = game;

    this.speed = {
      x:5,
      y:5,
    };

    this.position = {
      x: 0,
      y: 400,
    }

    this.size = 16;

  }

  draw(ctx) {
    ctx.drawImage(this.image, this.position.x, this.position.y, this.size, this.size);
  }

  update(delta_time) {

    this.position.x += this.speed.x;
    this.position.y += this.speed.y;

    //inverts the speed of the ball if it hits either x position
    if (this.position.x + this.size > this.game_width || this.position.x < 0){
      this.speed.x = -this.speed.x;
    }

    //inverts the speed of the ball if it hits either y position
    if (this.position.y < 0){
      this.speed.y = -this.speed.y;
    }

    //check for bottom of game and trigger game over
    if (this.position.y + this.size >= this.game_height){
      this.game.lives--;
      this.speed.y = -this.speed.y;
    }

    if (detect_collision(this, this.game.paddle)){
      this.speed.y = -this.speed.y;
    }
  }

  reset(){
    this.position = {
      x: 0,
      y: 400,
    }

    this.speed = {
      x:5,
      y:5,
    };
  }

}