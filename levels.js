import Brick from "/src/brick";

export function build_level(game, level){

  let bricks = [];
  level.forEach((row, row_index) => {
    row.forEach((brick, brick_index) => {
      if (brick === 1){
        let position = {
          x: 80 * brick_index,
          y: 75 + 24 * row_index,
        };
        bricks.push(new Brick(game, position));
      }
    });

  });
  return bricks;
}

export const level1 = [
  [1, 1, 0, 0, 1, 1, 0, 0, 1, 1],
]

export const level2 = [
  [1, 1, 0, 0, 1, 1, 0, 0, 1, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]