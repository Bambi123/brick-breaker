export default class Paddle {

  constructor(game) {

    this.game_width = game.game_width;
    this.game_height = game.game_height;

    this.width = 150;
    this.height = 30;
    this.max_speed = 10;
    this.speed = 0;

    this.position = {
      x: 100,
      y: this.game_height - this.height - 10
    };

  }

  move_left(){
    this.speed = -this.max_speed;
  }

  move_right(){
    this.speed = this.max_speed;
  }

  stop(){
    this.speed = 0;
  }

  draw(ctx) {

    ctx.fillStyle = "#0ff";
    ctx.fillRect(this.position.x, this.position.y, this.width, this.height);

  }

  update(delta_time) {
    this.position.x += this.speed;
    if (this.position.x < 0) this.position.x = 0;
    if (this.position.x + this.width > this.game_width) this.position.x = this.game_width - this.width;
  }

}
