import Paddle from "/src/paddle";
import InputHandler from "/src/input";
import Ball from "/src/ball";

import {build_level, level1} from "/src/levels";

const GAME_STATE = {
  PAUSED: 0,
  RUNNING: 1,
  MENU: 2,
  GAME_OVER: 3,
  NEW_LEVEL: 4,
  WINNER: 5,
};

export default class Game {

  constructor(game_width, game_height){
    this.game_width = game_width;
    this.game_height = game_height;

    this.game_state = GAME_STATE.MENU;
    this.paddle = new Paddle(this);
    this.ball = new Ball(this);
    new InputHandler(this.paddle, this);
    this.game_objects = [];
    this.bricks = [];

    this.lives = 3;

    this.levels = [level1];
    this.no_of_levels = 1;
    this.curr_level = 0;
  }

  start(){
    if (this.game_state !== GAME_STATE.MENU && this.game_state !== GAME_STATE.NEW_LEVEL) return;

    this.game_state = GAME_STATE.RUNNING;
    this.bricks = build_level(this, this.levels[this.curr_level]);
    this.game_objects = [this.paddle, this.ball];
  }

  update(delta_time){
    if (this.lives === 0) this.game_state = GAME_STATE.GAME_OVER;
    if (this.game_state !== GAME_STATE.RUNNING) return;

    if (this.bricks.length === 0){
      this.curr_level++;
      if (this.curr_level === this.no_of_levels) {
        this.game_state = GAME_STATE.WINNER;
        return
      } else {
        this.game_state = GAME_STATE.NEW_LEVEL;
        this.ball.reset();
        this.start();
      }
    }

    [...this.game_objects,...this.bricks].forEach((object) => object.update(delta_time));
    this.bricks = this.bricks.filter(object => !object.marked_for_remove);
  }

  draw(ctx){

    [...this.game_objects, ...this.bricks].forEach((object) => object.draw(ctx));

    if (this.game_state === GAME_STATE.PAUSED){

      ctx.rect(0, 0, this.game_width, this.game_height);
      ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
      ctx.fill();

      ctx.font = "30px Arial";
      ctx.fillStyle = "white";
      ctx.textAlign = "center";
      ctx.fillText("Paused", this.game_width/2, this.game_height/2);

    } else if (this.game_state === GAME_STATE.MENU){

      ctx.rect(0, 0, this.game_width, this.game_height);
      ctx.fillStyle = "rgba(0, 0, 0, 1)";
      ctx.fill();

      ctx.font = "30px Arial";
      ctx.fillStyle = "white";
      ctx.textAlign = "center";
      ctx.fillText("Press SpaceBar to Start", this.game_width/2, this.game_height/2);

    } else if (this.game_state === GAME_STATE.GAME_OVER){

      ctx.rect(0, 0, this.game_width, this.game_height);
      ctx.fillStyle = "rgba(0, 0, 0, 1)";
      ctx.fill();

      ctx.font = "30px Arial";
      ctx.fillStyle = "white";
      ctx.textAlign = "center";
      ctx.fillText("GAME OVER", this.game_width/2, this.game_height/2);

      ctx.font = "20px Arial";
      ctx.fillStyle = "white";
      ctx.textAlign = "center";
      ctx.fillText("Play again?", this.game_width/2, this.game_height/2 + 50);

    } else if (this.game_state === GAME_STATE.WINNER){

      ctx.rect(0, 0, this.game_width, this.game_height);
      ctx.fillStyle = "lime";
      ctx.fill();

      ctx.font = "30px Arial";
      ctx.fillStyle = "white";
      ctx.textAlign = "center";
      ctx.fillText("YOU WIN!", this.game_width/2, this.game_height/2);

      ctx.font = "20px Arial";
      ctx.fillStyle = "white";
      ctx.textAlign = "center";
      ctx.fillText("Play again?", this.game_width/2, this.game_height/2 + 50);

    }


  }

  toggle_pause(){
    if (this.game_state === GAME_STATE.PAUSED){
      this.game_state = GAME_STATE.RUNNING;
    } else {
      this.game_state = GAME_STATE.PAUSED;
     }

  }
}